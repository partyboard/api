import { gql } from "apollo-server";

const typeDefs = gql`
  scalar Date

  type Party {
    id: ID!
    name: String!
    host: String!
    messages: [Message!]!
    users: [String!]!
    musics: [Music!]!
    jwt: String
    me: String
  }

  type Message {
    id: ID!
    sender: String!
    content: String!
    date: Date!
  }

  type Music {
    id: ID!
    videoId: ID!
    title: String!
    channelTitle: String!
    duration: String!
    thumbnail: String!
    likers: [String!]!
    dislikers: [String!]!
  }

  type SearchResult {
    videoId: ID!
    title: String!
    channelTitle: String!
    duration: String!
    thumbnail: String!
  }

  type Query {
    getParty: Party!
    autocompleteSearch(query: String!): [String!]!
    searchMusic(query: String!): [SearchResult!]!
  }

  type Mutation {
    createParty(name: String!, host: String!): Party!
    closeParty: Party!
    joinParty(id: ID!, name: String!): Party!
    sendMessage(content: String!): Message!
    removeMessage(id: ID!): Message!
    addMusic(id: ID!): Music!
    removeMusic(id: ID!): Music!
    likeMusic(id: ID!): Music!
    dislikeMusic(id: ID!): Music!
    advancePlaylist: Music
    kickUser(user: String!): String!
  }

  type Subscription {
    messageSent(id: ID!): Message!
    messageRemoved(id: ID!): Message!
    userJoined(id: ID!): String!
    userLeft(id: ID!): String!
    musicAdded(id: ID!): Music!
    musicRemoved(id: ID!): Music!
    musicUpdated(id: ID!): Music!
    partyClosed(id: ID!): Party!
  }
`;

export default typeDefs;
