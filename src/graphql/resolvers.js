import {
  PubSub,
  withFilter,
  AuthenticationError,
  ApolloError,
} from "apollo-server";
import { autoCompleteQuery, searchVideo } from "../youtube";

export const pubsub = new PubSub();

const resolvers = {
  Query: {
    getParty: async (_parent, _args, { dataSources: { partyData }, user }) => {
      if (!user) {
        throw new AuthenticationError();
      }
      const party = await partyData.getParty(user.party);
      return {
        ...party.toObject({ virtuals: true }),
        users: party.users.includes(user.name)
          ? party.users
          : user.name === party.host
          ? [user.name, ...party.users]
          : [...party.users, user.name],
        me: user.name,
      };
    },
    autocompleteSearch: async (
      _parent,
      { query },
      { dataSources: { partyData }, user }
    ) => {
      if (!(await partyData.isUserInParty(user))) {
        throw new AuthenticationError();
      }
      return autoCompleteQuery(query);
    },
    searchMusic: async (
      _parent,
      { query },
      { dataSources: { partyData }, user }
    ) => {
      if (!(await partyData.isUserInParty(user))) {
        throw new AuthenticationError();
      }
      return searchVideo(query);
    },
  },
  Mutation: {
    createParty: (_parent, args, { dataSources: { partyData } }) => {
      return partyData.createParty(args);
    },
    closeParty: async (
      _parent,
      _args,
      { dataSources: { partyData }, user }
    ) => {
      if (!(await partyData.isUserInParty(user))) {
        throw new AuthenticationError();
      }
      const party = partyData.deleteParty(user);
      pubsub.publish("PARTY_CLOSED", { partyClosed: party });
      return party;
    },
    joinParty: (_parent, args, { dataSources: { partyData } }) => {
      return partyData.joinParty(args);
    },
    sendMessage: async (
      _parent,
      { content },
      { dataSources: { partyData, messageData }, user }
    ) => {
      if (!(await partyData.isUserInParty(user))) {
        throw new AuthenticationError();
      }
      const party = await partyData.getParty(user.party);
      const message = await messageData.createMessage(
        user.name,
        content,
        party
      );
      pubsub.publish("MESSAGE_SENT", { messageSent: message });
      return message;
    },
    removeMessage: async (
      _parent,
      { id },
      { dataSources: { partyData, messageData }, user }
    ) => {
      if (!(await partyData.isUserInParty(user))) {
        throw new AuthenticationError();
      }
      const party = await partyData.getParty(user.party);
      if (user.name != party.host) {
        throw new ApolloError("You must be the host to remove a message");
      }

      const message = await messageData.removeMessage(party, id);
      pubsub.publish("MESSAGE_REMOVED", { messageRemoved: message });
      return message;
    },
    addMusic: async (
      _parent,
      { id },
      { dataSources: { partyData, musicData }, user }
    ) => {
      if (!(await partyData.isUserInParty(user))) {
        throw new AuthenticationError();
      }
      const party = await partyData.getParty(user.party);
      const music = await musicData.createMusic(id, party);
      pubsub.publish("MUSIC_ADDED", { musicAdded: music });
      return music;
    },
    removeMusic: async (
      _parent,
      args,
      { dataSources: { partyData, musicData }, user }
    ) => {
      if (!(await partyData.isUserInParty(user))) {
        throw new AuthenticationError();
      }
      const party = await partyData.getParty(user.party);
      if (user.name != party.host) {
        throw new ApolloError("You must be the host to remove a music");
      }

      const music = musicData.removeMusicById(args.id, party);
      pubsub.publish("MUSIC_REMOVED", { musicRemoved: music });
      return music;
    },
    likeMusic: async (
      _parent,
      args,
      { dataSources: { partyData, musicData }, user }
    ) => {
      if (!(await partyData.isUserInParty(user))) {
        throw new AuthenticationError();
      }

      const music = musicData.likeMusic(args.id, user);
      pubsub.publish("MUSIC_UPDATED", { musicUpdated: music });
      return music;
    },
    dislikeMusic: async (
      _parent,
      args,
      { dataSources: { partyData, musicData }, user }
    ) => {
      if (!(await partyData.isUserInParty(user))) {
        throw new AuthenticationError();
      }

      const music = musicData.dislikeMusic(args.id, user);
      pubsub.publish("MUSIC_UPDATED", { musicUpdated: music });
      return music;
    },
    advancePlaylist: async (
      _parent,
      _args,
      { dataSources: { partyData, musicData }, user }
    ) => {
      if (!(await partyData.isUserInParty(user))) {
        throw new AuthenticationError();
      }

      const party = await partyData.getParty(user.party);
      if (user.name != party.host) {
        throw new ApolloError("You must be the host to advance the playlist");
      }

      if (party.musics.length > 0) {
        const music = musicData.removeMusic(party.musics[0], party);
        pubsub.publish("MUSIC_REMOVED", { musicRemoved: music });
        return music;
      }
      return null;
    },
    kickUser: async (
      _parent,
      { user: userToKick },
      { dataSources: { partyData }, user }
    ) => {
      if (!(await partyData.isUserInParty(user))) {
        throw new AuthenticationError();
      }

      const party = await partyData.getParty(user.party);
      if (user.name != party.host) {
        throw new ApolloError("You must be the host to kick a user");
      }

      partyData.leaveParty({ party: user.party, name: userToKick });
      pubsub.publish("USER_LEFT", { userLeft: userToKick });
      return userToKick;
    },
  },
  Subscription: {
    messageSent: {
      subscribe: withFilter(
        () => pubsub.asyncIterator("MESSAGE_SENT"),
        (_payload, { id }, { user }) => {
          return id === user.party;
        }
      ),
    },
    messageRemoved: {
      subscribe: withFilter(
        () => pubsub.asyncIterator("MESSAGE_REMOVED"),
        (_payload, { id }, { user }) => {
          return id === user.party;
        }
      ),
    },
    userJoined: {
      subscribe: withFilter(
        () => pubsub.asyncIterator("USER_JOINED"),
        (_payload, { id }, { user }) => {
          return id === user.party;
        }
      ),
    },
    userLeft: {
      subscribe: withFilter(
        () => pubsub.asyncIterator("USER_LEFT"),
        (_payload, { id }, { user }) => {
          return id === user.party;
        }
      ),
    },
    musicAdded: {
      subscribe: withFilter(
        () => pubsub.asyncIterator("MUSIC_ADDED"),
        (_payload, { id }, { user }) => {
          return id === user.party;
        }
      ),
    },
    musicRemoved: {
      subscribe: withFilter(
        () => pubsub.asyncIterator("MUSIC_REMOVED"),
        (_payload, { id }, { user }) => {
          return id === user.party;
        }
      ),
    },
    musicUpdated: {
      subscribe: withFilter(
        () => pubsub.asyncIterator("MUSIC_UPDATED"),
        (_payload, { id }, { user }) => {
          return id === user.party;
        }
      ),
    },
    partyClosed: {
      subscribe: withFilter(
        () => pubsub.asyncIterator("PARTY_CLOSED"),
        (_payload, { id }, { user }) => {
          return id === user.party;
        }
      ),
    },
  },
};

export default resolvers;
