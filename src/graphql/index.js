import resolvers, { pubsub } from "./resolvers";
import typeDefs from "./type-defs";

export { resolvers, pubsub, typeDefs };
