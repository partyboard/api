import { MongoDataSource } from "apollo-datasource-mongodb";
import { ApolloError, AuthenticationError } from "apollo-server";
import { generateToken } from "../jwt";

export default class Party extends MongoDataSource {
  async getParty(id) {
    return await this.findOneById(id);
  }

  async createParty({ name, host }) {
    const party = await this.model.create({ name, host });
    return {
      ...party.toObject({ virtuals: true }),
      jwt: generateToken(host, party.id),
    };
  }

  async deleteParty(user) {
    const party = await this.findOneById(user.party);
    if (!party) {
      throw new ApolloError("Party not found");
    }
    if (user.name !== party.host) {
      throw new AuthenticationError("You are not the host of this party");
    }
    party.messages.forEach((message) => message.deleteOne());
    party.musics.forEach((music) => music.deleteOne());
    party.deleteOne();
    return party;
  }

  async joinParty({ id, name }) {
    const party = await this.findOneById(id);
    if (!party) {
      throw new ApolloError("Party not found");
    }
    const userAlreadyExists = party.users.includes(name);
    if (userAlreadyExists || name === party.host) {
      throw new ApolloError("This username is not available");
    }
    if (name === party.host) {
      party.users.unshift(name);
    } else {
      party.users.push(name);
    }
    party.save();
    return {
      ...party.toObject({ virtuals: true }),
      jwt: generateToken(name, party.id),
    };
  }

  async resumeParty({ party: id, name }) {
    const party = await this.model.findById(id);
    if (!party) {
      return;
    }
    const userAlreadyExists = party.users.includes(name);
    if (!userAlreadyExists) {
      if (name === party.host) {
        party.users.unshift(name);
      } else {
        party.users.push(name);
      }
      party.save();
    }
    return party;
  }

  async leaveParty({ party: id, name }) {
    const party = await this.model.findById(id);
    if (!party) {
      return;
    }
    const userExists = party.users.includes(name);
    if (userExists) {
      party.users.splice(party.users.indexOf(name), 1);
      party.save();
    }
    return party;
  }

  async isUserInParty(user) {
    if (!user) {
      return false;
    }
    const party = await this.findOneById(user.party);
    if (!party) {
      return false;
    }
    return party.users.includes(user.name);
  }
}
