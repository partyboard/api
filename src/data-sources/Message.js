import { ApolloError } from "apollo-server";
import { MongoDataSource } from "apollo-datasource-mongodb";

export default class Message extends MongoDataSource {
  async createMessage(sender, content, party) {
    const message = await this.model.create({
      sender,
      content,
      date: new Date(),
    });
    party.messages.push(message._id);
    party.save();
    return message;
  }

  async removeMessage(party, id) {
    const message = await this.findOneById(id);
    if (!message) {
      throw new ApolloError("Message not found");
    }
    if (!party.messages.some((message) => message.id === id)) {
      throw new ApolloError("Message not in this party");
    }
    party.messages = party.messages.filter((message) => message.id !== id);
    party.save();

    message.deleteOne();
    return message;
  }
}
