import { MongoDataSource } from "apollo-datasource-mongodb";
import { ApolloError } from "apollo-server";
import { get } from "mongoose";
import { getVideoDetails } from "../youtube";

export default class Message extends MongoDataSource {
  async createMusic(videoId, party) {
    const videoDetails = await getVideoDetails(videoId);
    if (!videoDetails) {
      throw ApolloError("Video not found");
    }

    const music = await this.model.create({
      videoId,
      title: videoDetails.title,
      channelTitle: videoDetails.channelTitle,
      duration: videoDetails.duration,
      thumbnail: videoDetails.thumbnail,
    });

    party.musics.push(music.id);
    party.save();

    return music;
  }

  removeMusic(music, party) {
    party.musics = party.musics.filter((m) => m.id !== music.id);
    party.save();

    music.deleteOne();
    return music;
  }

  async removeMusicById(id, party) {
    const music = await this.findOneById(id);
    if (!music) {
      throw ApolloError("Music not found");
    }

    party.musics = party.musics.filter((m) => m.id !== music.id);
    party.save();

    music.deleteOne();
    return music;
  }

  async likeMusic(id, user) {
    const music = await this.findOneById(id);
    if (!music) {
      throw ApolloError("Music not found");
    }

    if (!music.likers.includes(user.name)) {
      music.likers.push(user.name);
    }
    if (music.dislikers.includes(user.name)) {
      music.dislikers.splice(music.dislikers.indexOf(user.name), 1);
    }

    music.save();
    return music;
  }

  async dislikeMusic(id, user) {
    const music = await this.findOneById(id);
    if (!music) {
      throw ApolloError("Music not found");
    }

    if (music.likers.includes(user.name)) {
      music.likers.splice(music.likers.indexOf(user.name), 1);
    }
    if (!music.dislikers.includes(user.name)) {
      music.dislikers.push(user.name);
    }

    music.save();
    return music;
  }
}
