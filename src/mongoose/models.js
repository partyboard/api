import mongoose from "mongoose";
import { partySchema, messageSchema, musicSchema } from "./schema";

export const PartyModel = mongoose.model("Party", partySchema);

export const MessageModel = mongoose.model("Message", messageSchema);

export const MusicModel = mongoose.model("Music", musicSchema);
