import { Schema } from "mongoose";
import mongooseAutopopulate from "mongoose-autopopulate";

export const partySchema = new Schema({
  name: String,
  host: String,
  messages: [
    { type: Schema.Types.ObjectId, ref: "Message", autopopulate: true },
  ],
  users: [String],
  musics: [{ type: Schema.Types.ObjectId, ref: "Music", autopopulate: true }],
});

partySchema.plugin(mongooseAutopopulate);

export const messageSchema = new Schema({
  sender: String,
  content: String,
  date: Date,
});

export const musicSchema = new Schema({
  videoId: String,
  title: String,
  channelTitle: String,
  duration: String,
  thumbnail: String,
  likers: [String],
  dislikers: [String],
});
