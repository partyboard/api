import dotenv from "dotenv";
import mongoose from "mongoose";
import { ApolloServer } from "apollo-server";
import { typeDefs, resolvers, pubsub } from "./graphql";
import { verifyToken } from "./jwt";
import {
  PartyDataSource,
  MessageDataSource,
  MusicDataSource,
} from "./data-sources";
import { PartyModel, MessageModel, MusicModel } from "./mongoose/models";
import Agenda from "agenda";

async function main() {
  dotenv.config();

  await mongoose.connect(process.env.MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  const agenda = new Agenda({ mongo: mongoose.connection });

  agenda.define("delete empty parties", async () => {
    const emptyParties = await PartyModel.find({ users: [] });
    emptyParties.forEach((party) => {
      party.messages.forEach((message) => message.deleteOne());
      party.musics.forEach((music) => music.deleteOne());
      party.deleteOne();
      pubsub.publish("PARTY_CLOSED", { partyClosed: party });
    });
  });

  agenda.start();
  agenda.every("1 hour", "delete empty parties");

  const server = new ApolloServer({
    typeDefs,
    resolvers,
    dataSources: () => ({
      partyData: new PartyDataSource(PartyModel),
      messageData: new MessageDataSource(MessageModel),
      musicData: new MusicDataSource(MusicModel),
    }),
    context: ({ req, connection }) => {
      if (connection) {
        return connection.context;
      }
      const token = req.headers.authorization || "";
      const user = verifyToken(token);
      return { user };
    },
    subscriptions: {
      keepAlive: 25000,
      onConnect: (connectionParams) => {
        const partyData = new PartyDataSource(PartyModel);
        const token = connectionParams.authorization || "";
        const user = verifyToken(token);
        if (!user) {
          return false;
        }
        partyData.resumeParty(user);
        pubsub.publish("USER_JOINED", { userJoined: user.name });
        return { user, partyData };
      },
      onDisconnect: async (_websocket, { initPromise }) => {
        const initialContext = await initPromise;
        if (initialContext.user && initialContext.partyData) {
          initialContext.partyData.leaveParty(initialContext.user);
          pubsub.publish("USER_LEFT", { userLeft: initialContext.user.name });
        }
      },
    },
  });

  server.listen({ port: process.env.PORT || 4000 }).then(({ url }) => {
    console.log(`Server ready at ${url}`);
  });
}

main().catch((err) => {
  console.error(err);
  process.exit(1);
});
