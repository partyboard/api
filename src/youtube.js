import fetch from "node-fetch";
import { URL, URLSearchParams } from "url";
import memoize from "fast-memoize";

const convertDuration = (duration) => {
  const timeExtractor = /([0-9]*H)?([0-9]*M)?([0-9]*S)?$/;
  const extracted = timeExtractor.exec(duration);
  const hours = parseInt(extracted[1], 10) || 0;
  let minutes = parseInt(extracted[2], 10) || 0;
  let seconds = parseInt(extracted[3], 10) || 0;
  if (hours > 0) {
    minutes = minutes.toString().padStart(2, "0");
  }
  if (minutes > 0) {
    seconds = seconds.toString().padStart(2, "0");
  }
  return [hours, minutes, seconds].filter((v) => v > 0).join(":");
};

export const autoCompleteQuery = async (query) => {
  if (query.length === 0) {
    return [];
  }
  const url = new URL("https://suggestqueries.google.com/complete/search");
  url.search = new URLSearchParams({
    client: "firefox",
    ds: "yt",
    q: query,
  }).toString();
  const data = await fetch(url).then((res) => res.json());
  if (data.length > 2) {
    return data[1];
  }
  return [];
};

export const searchVideo = memoize(async (query) => {
  const searchUrl = new URL("https://www.googleapis.com/youtube/v3/search");
  searchUrl.search = new URLSearchParams({
    part: "snippet",
    type: "video",
    videoEmbeddable: true,
    videoDuration: "short",
    maxResults: 25,
    key: process.env.YOUTUBE_API_KEY,
    q: query,
  }).toString();
  const searchData = await fetch(searchUrl).then((res) => res.json());

  const listUrl = new URL("https://www.googleapis.com/youtube/v3/videos");
  listUrl.search = new URLSearchParams({
    part: "contentDetails",
    id: searchData.items.map((item) => item.id.videoId).join(","),
    maxResults: 25,
    key: process.env.YOUTUBE_API_KEY,
  }).toString();
  const listData = await fetch(listUrl).then((res) => res.json());

  return searchData.items.map((item, index) => ({
    videoId: item.id.videoId,
    title: item.snippet.title,
    channelTitle: item.snippet.channelTitle,
    duration: convertDuration(listData.items[index].contentDetails.duration),
    thumbnail: item.snippet.thumbnails.medium.url,
  }));
});

export const getVideoDetails = memoize(async (videoId) => {
  const url = new URL("https://www.googleapis.com/youtube/v3/videos");
  url.search = new URLSearchParams({
    part: "snippet,contentDetails",
    id: videoId,
    key: process.env.YOUTUBE_API_KEY,
  }).toString();
  const data = await fetch(url).then((res) => res.json());

  if (data.items.length === 0) {
    return null;
  }

  return {
    videoId: data.items[0].id.videoId,
    title: data.items[0].snippet.title,
    channelTitle: data.items[0].snippet.channelTitle,
    duration: convertDuration(data.items[0].contentDetails.duration),
    thumbnail: data.items[0].snippet.thumbnails.medium.url,
  };
});
