import crypto from "crypto";
import jwt from "jsonwebtoken";

const secretKey =
  process.env.NODE_ENV == "dev" ? "partyboard" : crypto.randomBytes(256);

export const generateToken = (name, party) => {
  return jwt.sign({ name, party }, secretKey);
};

export const verifyToken = (token) => {
  try {
    return jwt.verify(token, secretKey);
  } catch {
    return null;
  }
};
